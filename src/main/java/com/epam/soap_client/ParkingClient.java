package com.epam.soap_client;

import javax.xml.bind.JAXBElement;

import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import parking.wsdl.AddDeal;
import parking.wsdl.AddDealResponse;
import parking.wsdl.Deal;
import parking.wsdl.GetCars;
import parking.wsdl.GetCarsResponse;
import parking.wsdl.GetDeals;
import parking.wsdl.GetDealsResponse;
import parking.wsdl.ObjectFactory;

public class ParkingClient extends WebServiceGatewaySupport {

  public GetCarsResponse getCarsResponse() {
    ObjectFactory factory = new ObjectFactory();
    GetCars request = factory.createGetCars();
    WebServiceTemplate t = getWebServiceTemplate();
    JAXBElement<GetCarsResponse> element =
        (JAXBElement<GetCarsResponse>) t.marshalSendAndReceive(factory.createGetCars(request));
    return element.getValue();
  }

  public AddDealResponse addDeal(Deal deal) {
    ObjectFactory factory = new ObjectFactory();
    AddDeal addDeal = factory.createAddDeal();
    addDeal.setArg0(deal);
    JAXBElement<AddDeal> request = factory.createAddDeal(addDeal);
    JAXBElement<AddDealResponse> response =
        (JAXBElement<AddDealResponse>) getWebServiceTemplate().marshalSendAndReceive(request);
    return response.getValue();
  }
  
  public GetDealsResponse getDealsResponse() {
    ObjectFactory factory = new ObjectFactory();
    GetDeals request = factory.createGetDeals();
    WebServiceTemplate t = getWebServiceTemplate();
    JAXBElement<GetDealsResponse> element =
        (JAXBElement<GetDealsResponse>) t.marshalSendAndReceive(factory.createGetDeals(request));
    return element.getValue();
  }
}
