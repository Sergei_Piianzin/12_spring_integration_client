package com.epam.soap_client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import parking.wsdl.AddDeal;
import parking.wsdl.Car;
import parking.wsdl.Deal;
import parking.wsdl.GetCarsResponse;

@SpringBootApplication
public class ParkingClientApplication implements CommandLineRunner {

  public static void main(String[] args) {
    SpringApplication.run(ParkingClientApplication.class, args);
  }

  @Autowired
  private ParkingClient parkingClient;

  @Override
  public void run(String... args) throws Exception {
    List<Car> cars = parkingClient.getCarsResponse().getReturn();
    for (Car c : cars) {
      System.out.printf("Car [model_id: %s,number: %s] %n", c.getModelId(), c.getNumber());
    }

    List<Deal> deals = parkingClient.getDealsResponse().getReturn();

    for (Deal d : deals) {
      System.out.printf("Deal [id: %s,client_id: %s, parking_number: %s, employee_id: %s] %n",
          d.getId(), d.getClientId(), d.getParkingNumber(), d.getEmployeeId());
    }
    Deal deal = new Deal();
    deal.setId(1);
    deal.setParkingNumber(5);
    deal.setEmployeeId(2);
    deal.setClientId(1);

    parkingClient.addDeal(deal);
    
    deals = parkingClient.getDealsResponse().getReturn();

    for (Deal d : deals) {
      System.out.printf("Deal [id: %s,client_id: %s, parking_number: %s, employee_id: %s] %n",
          d.getId(), d.getClientId(), d.getParkingNumber(), d.getEmployeeId());
    }
  }
}
